package invoice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

public class InvoiceRowGeneratorTest {

    @Mock
    InvoiceRowDao dao;

    @Test
    public void whenPeriodStartsBeforeTheTenth_TheFirstPaymentIsOnTheTenth() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2018-01-09");
        LocalDate periodEnd = asDate("2018-02-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-01-10"))));
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-02-10"))));
    }

    @Test
    public void whenPeriodStartsOnTheTenth_TheFirstPaymentIsOnTheSameDay() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2018-01-10");
        LocalDate periodEnd = asDate("2018-02-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-01-10"))));
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-02-10"))));
    }

    @Test
    public void whenPeriodStartsAfterTheTenth_TheFirstPaymentIsOnTheSameDay() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2018-01-11");
        LocalDate periodEnd = asDate("2018-02-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-01-11"))));
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-02-10"))));
    }

    @Test
    public void whenPeriodEndsBeforeTheTenth_ThatMonthHasNoPayment() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2018-01-10");
        LocalDate periodEnd = asDate("2018-02-09");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-01-10"))));
    }

    @Test
    public void newYearDoesNotInterfereWithCalculatingDatesCorrectly() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-12-01");
        LocalDate periodEnd = asDate("2018-02-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2017-12-10"))));
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-01-10"))));
        verify(dao).save(argThat(ir -> ir.date.equals(asDate("2018-02-10"))));
    }

    @Test
    public void sumIsDividedEquallyIfPossible() {
        BigDecimal amount = new BigDecimal(9);
        LocalDate periodStart = asDate("2018-01-10");
        LocalDate periodEnd = asDate("2018-03-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao, times(3)).save(argThat(ir -> ir.amount.intValue() == 3));
    }

    @Test
    public void remainderOfOneIsAddedToTheLastPayment() {
        InOrder inOrder = inOrder(dao);
        BigDecimal amount = new BigDecimal(7);
        LocalDate periodStart = asDate("2018-01-10");
        LocalDate periodEnd = asDate("2018-02-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        inOrder.verify(dao).save(argThat(ir -> ir.amount.intValue() == 3));
        inOrder.verify(dao).save(argThat(ir -> ir.amount.intValue() == 4));
    }

    @Test
    public void remainderMoreThanOneIsDividedBetweenLastElements() {
        InOrder inOrder = inOrder(dao);
        BigDecimal amount = new BigDecimal(11);
        LocalDate periodStart = asDate("2018-01-10");
        LocalDate periodEnd = asDate("2018-03-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        inOrder.verify(dao).save(argThat(ir -> ir.amount.intValue() == 3));
        inOrder.verify(dao, times(2)).save(argThat(ir -> ir.amount.intValue() == 4));
    }

    @Test
    public void ifSomePaymentsWouldBeLessThanThreeEuros_ThePeriodIsShortened() {
        BigDecimal amount = new BigDecimal(6);
        LocalDate periodStart = asDate("2018-01-10");
        LocalDate periodEnd = asDate("2018-03-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao, times(2)).save(argThat(ir -> ir.amount.intValue() == 3));
    }

    @Test
    public void aTotalOfUnderThreeEurosIsPaidAtOnce() {
        BigDecimal amount = new BigDecimal(2);
        LocalDate periodStart = asDate("2018-01-10");
        LocalDate periodEnd = asDate("2018-02-10");
        InvoiceRowGenerator generator = new InvoiceRowGenerator(dao);
        generator.generateInvoiceRows(amount, periodStart, periodEnd);
        verify(dao).save(argThat(ir -> ir.amount.intValue() == 2));
    }

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private LocalDate asDate(String string) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(string, formatter);
    }

}