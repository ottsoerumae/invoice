package invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class InvoiceRowGenerator {
    InvoiceRowDao dao;
    private final int PAYMENT_DAY_OF_MONTH = 10;
    private List<LocalDate> paymentDates = new ArrayList<>();
    private Queue<Integer> paymentAmounts = new LinkedList<>();

    private int getNumberOfPaymentDates() {
        return paymentDates.size();
    }

    public InvoiceRowGenerator(InvoiceRowDao dao) {
        this.dao = dao;
    }

    public void generateInvoiceRows(BigDecimal amount, LocalDate periodStart, LocalDate periodEnd) {
        paymentDates = generateDatesOfPayments(periodStart, periodEnd);
        generateAmountsForPayments(amount);
        for (LocalDate paymentDate : paymentDates) {
            if(!paymentAmounts.isEmpty()) {
                dao.save(new InvoiceRow(new BigDecimal(paymentAmounts.poll()), paymentDate));
            }
        }
    }

    private List<LocalDate> generateDatesOfPayments(LocalDate periodStart, LocalDate periodEnd) {
        List<LocalDate> datesOfPayments = new ArrayList<>();
        LocalDate firstPaymentDate = LocalDate.of(periodStart.getYear(), periodStart.getMonth(), getFirstPaymentDayOfMonth(periodStart));
        datesOfPayments.add(firstPaymentDate);
        LocalDate paymentDate = firstPaymentDate.plusMonths(1);
        paymentDate = LocalDate.of(paymentDate.getYear(), paymentDate.getMonth(), PAYMENT_DAY_OF_MONTH);
        while(paymentDate.isBefore(periodEnd)|| paymentDate.equals(periodEnd)) {
            datesOfPayments.add(paymentDate);
            paymentDate = paymentDate.plusMonths(1);
        }
        return datesOfPayments;
    }

    private void generateAmountsForPayments(BigDecimal invoiceTotal) {
        int total = invoiceTotal.intValue();
        int numberOfPaymentsLeft = getNumberOfPaymentDates();
        if(total < 3) {
            paymentAmounts.add(total);
            return;
        }
        if(total/numberOfPaymentsLeft < 3) {
            numberOfPaymentsLeft = total / 3;
        }
        while (numberOfPaymentsLeft > 0) {
            int amount = total / numberOfPaymentsLeft;
            paymentAmounts.add(amount);
            total = total - amount;
            numberOfPaymentsLeft--;
        }
    }

    private int getFirstPaymentDayOfMonth(LocalDate periodStart) {
        if (periodStart.getDayOfMonth() <= 10) {
            return 10;
        }
        else {
            return periodStart.getDayOfMonth();
        }
    }
}