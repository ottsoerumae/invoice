This small project shows my solution to a simple task to divide a given loan into several instalments.
The number of instalments is specified by the start and end dates of the period
Each instalment consists of an amount to be paid and a payment date.
The program is built to match the following prerequisites:
1. The instalment takes place on the 10th day of the month.
2. If the period starts after the 10th, the first instalment is on the same day
3. When the period ends before the 10th, the last payment is on the previous month
4. Sum is divided equally, but if not possible, then the remainder is divided between the last months.
5. The minimal amount of the payment is 3 Euros
6. If an average instalment amount is less than 3 Euros, the period is shortened